# Spot a Grandis

This app is developed to help track the progress of the invasive species of 
Phelsuma Grandis in Mauritius (and potentially other islands in the indian 
ocean). It is based on the wonderful Quasar framework and is compiled using
Cordova to become a semi-native app.
